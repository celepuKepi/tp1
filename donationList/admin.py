from django.contrib import admin

# Register your models here.
from donationList.models import Donation
#
admin.site.register(Donation)