from datetime import datetime

from django.test import TestCase
from donationList.models import Donation

class TestModel(TestCase):

    def setUp(self):
        Donation.objects.create(program='banjir',
                                target=50000000,
                                total=50000,
                                news='testtest',
                                description="testtest")

    def test_var_of_Donations_is_exist(self):
        donation = Donation.objects.get(id=1)
        self.assertEqual(donation.program, 'banjir')
        self.assertLess(40000000,donation.target)
        self.assertGreater(donation.total,0)