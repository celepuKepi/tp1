from django.test import TestCase, Client
from django.urls import resolve

from donationList.views import donations


class TestURl(TestCase):
    def test_donationListPage_is_exist(self):
        page = resolve('/donations/')
        self.assertEqual(page.func, donations)
    def test_donation_html_is_used(self):
        response = self.client.get('/donations/')
        self.assertEqual(response.status_code, 200)