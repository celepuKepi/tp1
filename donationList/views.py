from django.shortcuts import render
# Create your views here.
from .models import Donation


def donations(request):
    news = Donation.objects.all()
    response = {'news':news}
    return render(request, 'donations.html', response)