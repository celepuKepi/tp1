from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from .models import Signup
from .forms import Formregister


class Story6test(TestCase):

    def test_Story6_url_is_exist(self):
        response = Client().get('/register/member/')
        self.assertEqual(response.status_code,200)

    def test_Story6_using_index_template(self):
        response = Client().get('/register/member/')
        self.assertTemplateUsed(response, 'register.html')
    	
    def test_Story6_using_addStatus_func(self):
        found = resolve('/register/member/')
        self.assertEqual(found.func,register)
    
    def test_Story6_using_addStatus_func(self):
        found = resolve('/register/member/')
        self.assertEqual(found.func,Member)
    
    def test_model_can_create_new_status(self):
        new_nama = Signup.objects.create(Nama='Isi Nama')
        new_Email = Signup.objects.create(Email='Isi email')
        new_Password = Signup.objects.create(Password='Isi password')
        counting_all_available_activity = Signup.objects.all().count()
        self.assertEqual(counting_all_available_activity,3)

    # def test_can_save_a_POST_request(self):
    #     response = self.client.post('/', data={'judul_Status': 'ini judul', 'isi_Status' : 'aku mau latihan ngoding dehh'})
    #     counting_all_available_activity = Status.objects.all().count()
    #     self.assertEqual(counting_all_available_activity, 1)
    #     self.assertEqual(response.status_code, 302)
    #     self.assertEqual(response['location'], '/')
    #     new_response = self.client.get('/')
    #     html_response = new_response.content.decode('utf8')
    #     self.assertIn('aku mau latihan ngoding dehh', html_response)

    def test_Story6_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/register/member/', {'Nama': test,'Email' : test ,'Password' : test})
        self.assertEqual(response_post.status_code, 302)
    
            #response= Client().get('/')
            #html_response = response.content.decode('utf8')
            #self.assertIn(test, html_response)

    # def test_Story6_post_error_and_render_the_result(self):
    #         test = 'Anonymous'
    #         response_post = Client().post('/', { 'Status_Kamu': ''})
    #         self.assertEqual(response_post.status_code, 302)
    
    #         response= Client().get('/')
    #         html_response = response.content.decode('utf8')
    #         self.assertNotIn(test, html_response)

    # def test_string_representation(self):
    #     string = Status(judul_Status="ngoding kuy")
    #     self.assertEqual(str(string), string.judul_Status)

    #def test_create_anything_model(self,  isi='ini isi dtatus'):
    #	return Signup.objects.create( Status_Kamu=isi)
    #dasdiasoidsaijdiosajdoaj

    

 


