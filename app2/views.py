from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from .models import Signup
from .forms import Formregister
from django.contrib import messages
def register(request):
    return render(request, 'register.html')
def Member(request):
	if request.method == "POST":
		form = Formregister(request.POST)
		if form.is_valid():
			Signup(Nama =form.cleaned_data['Nama'],
				Email = form.cleaned_data['Email'],
				Password = form.cleaned_data['Password']).save()
		return HttpResponseRedirect('/')
	else:
		form = Formregister()
		dataMember = Signup.objects.all()
		context = {
		'Membernya' : dataMember,
		}
	return render(request,'register.html',{'form': form,'Membernya' : dataMember})
