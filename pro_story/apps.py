from django.apps import AppConfig


class ProStoryConfig(AppConfig):
    name = 'pro_story'
