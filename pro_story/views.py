from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.utils.datastructures import MultiValueDictKeyError

from donationList.models import Donation
from pro_story.forms import DonationForm
from pro_story.models import DonationIn

response = {}

# Create your views here.
def story(request, program):
    getProgram = Donation.objects.get(program=program)
    response = {'program':getProgram}
    return render(request, 'story.html', response)

def donationinput(request):
    response['savedonation'] = DonationForm()
    return render(request,'donationinput.html',response)

def savedonation(request):
    form = DonationForm(request.POST or None)
    if(request.method == 'POST'):
        response['amount'] = request.POST['amount']
        response['email'] = request.POST['email']
        try:
            is_showable = request.POST['is_private']
        except MultiValueDictKeyError:
            is_showable = False
        response['savedonation'] = form
        donate = DonationIn(
            amount=response['amount'],
            fullname = request.POST['fullname'],
            donatur_showable = not is_showable,
            email=response['email']
            )
        donate.save()
        return HttpResponseRedirect('/story/confirmation/')
    else:
        return HttpResponseRedirect('/')

def confirmation(request):
    confirm = DonationIn.objects.all()
    response['confirm'] = confirm
    return render(request,'confirmation.html',response)
