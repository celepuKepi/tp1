from django.test import TestCase
from django.test import Client
from django.http import HttpRequest
from django.urls import resolve
from pro_story.views import story, donationinput, confirmation
from .models import DonationIn

# Create your tests here.
class TestStory(TestCase):
    #URL Test
    def test_storypage_is_exist(self):
        page = resolve('/story/testttt/')
        self.assertEqual(page.func,story)
    def test_storypage_html_is_used(self):
        response = Client().get('/story/')
        self.assertEqual(response.status_code,404)
    def test_donationinputpage_is_exist(self):
        page = resolve('/story/donate/')
        self.assertEqual(page.func,donationinput)
    def test_donationinputpage_html_is_used(self):
        response = Client().get('/story/donate/')
        self.assertEqual(response.status_code,200)
    def test_confirmationpage_is_exist(self):
        page = resolve('/story/confirmation/')
        self.assertEqual(page.func,confirmation)
    def test_confirmationpage_html_is_used(self):
        response = Client().get('/story/confirmation/fafafa')
        self.assertEqual(response.status_code,404)

    #Model TestURl
    def test_model_can_create_new_input(self):
        #Creating new status
        new_status = DonationIn.objects.create(amount=100000, fullname='Kak Pewe', email='kakpewe@pewe.com')
        # Retrieving status that has been made
        counting_all_status = DonationIn.objects.all().count()
        self.assertEqual(counting_all_status, 1)
