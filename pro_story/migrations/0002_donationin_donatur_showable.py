

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pro_story', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='donationin',
            name='donatur_showable',
            field=models.BooleanField(default=False),
        ),
    ]
