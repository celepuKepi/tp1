from django.db import models

# Create your models here.
class DonationIn(models.Model):
    amount = models.DecimalField(max_digits=10,decimal_places=2,default=0)
    fullname = models.CharField(max_length=30, default='')
    email = models.CharField(max_length=50,default='')
    donatur_showable = models.BooleanField(default = False)
