"""pro_fund URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url

from app2 import views as memberviews
from django.contrib import admin
from django.urls import path, include
from donationList import views
from pro_story import views
from homepage import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('donations/', include('donationList.urls')),
    path('story/',include('pro_story.urls')),
    path('register/', include('app2.urls')),
    path('', include('homepage.urls')),

]



